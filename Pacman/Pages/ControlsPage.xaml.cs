﻿// <copyright file="ControlsPage.xaml.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for ControlsPage.xaml
    /// </summary>
    public partial class ControlsPage : Page
    {
        private MainWindow window;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlsPage"/> class.
        /// </summary>
        /// <param name="window">window parameter</param>
        public ControlsPage(MainWindow window)
        {
            this.InitializeComponent();
            this.window = window;
        }

        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Label).FontSize = 36;

            // (sender as Label).FontSize = 32;
        }

        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Label).FontSize = 32;

            // (sender as Label).Foreground = Brushes.White;
        }

        private void OnBack(object sender, MouseButtonEventArgs e)
        {
            this.window.SetPage(new MainMenu(this.window));
        }
    }
}
