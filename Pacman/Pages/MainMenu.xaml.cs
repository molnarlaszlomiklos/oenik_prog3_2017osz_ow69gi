﻿// <copyright file="MainMenu.xaml.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Page
    {
        private MainWindow window;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        /// <param name="window">Parameter to select running window</param>
        public MainMenu(MainWindow window)
        {
            this.InitializeComponent();
            this.window = window;
        }

        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Label).FontSize = 36;

            // (sender as Label).FontSize = 32;
        }

        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Label).FontSize = 32;

            // (sender as Label).Foreground = Brushes.White;
        }

        private void OnExit(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void OnAboutPageClick(object sender, MouseButtonEventArgs e)
        {
            this.window.SetPage(new AboutPage(this.window));
        }

        private void OnControlPageClick(object sender, MouseButtonEventArgs e)
        {
            this.window.SetPage(new AboutPage(this.window));
        }

        private void OnGamePageClick(object sender, MouseButtonEventArgs e)
        {
            this.window.SetPage(new GamePage(this.window));
        }
    }
}
