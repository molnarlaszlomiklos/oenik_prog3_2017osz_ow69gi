﻿// <copyright file="GamePage.xaml.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for GamePage.xaml
    /// </summary>
    public partial class GamePage : Page
    {
        /// <summary>
        /// Window, to load a page.
        /// </summary>
        private MainWindow window;

        /// <summary>
        /// Initializes a new instance of the <see cref="GamePage"/> class.
        /// </summary>
        /// <param name="window">Parameter to give a MainWindow, which contains page.</param>
        public GamePage(MainWindow window)
        {
            this.InitializeComponent();

            this.window = new MainWindow();
            this.window = window;
        }
    }
}
