﻿// <copyright file="AboutPage.xaml.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for AboutPage.xaml
    /// </summary>
    public partial class AboutPage : Page
    {
        private MainWindow window;

        /// <summary>
        /// Initializes a new instance of the <see cref="AboutPage"/> class.
        /// </summary>
        /// <param name="window">window parameter</param>
        public AboutPage(MainWindow window)
        {
            this.InitializeComponent();
            this.window = window;
        }

        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Label).FontSize = 36;

            // (sender as Label).FontSize = 32;
        }

        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Label).FontSize = 32;

            // (sender as Label).Foreground = Brushes.White;
        }

        private void OnBack(object sender, MouseButtonEventArgs e)
        {
            this.window.SetPage(new MainMenu(this.window));
        }
    }
}
