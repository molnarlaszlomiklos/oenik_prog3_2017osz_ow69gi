﻿// <copyright file="MainMenuAnimationVisual.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// Animation Visual
    /// </summary>
    public partial class MainMenuAnimationVisual : FrameworkElement
    {
        private const double SPEED = 10;

        private DispatcherTimer dt;
        private DispatcherTimer animation;

        private PacmanModel pacman;
        private BilagitModel bilagit;

        private GhostModel inky;
        private GhostModel blinky;
        private GhostModel pinky;
        private GhostModel clyde;

        private bool isPacmanHitBilagit = false;
        private bool isPacmanHitInky = false;
        private bool isPacmanHitPinky = false;
        private bool isPacmanHitBlinky = false;
        private bool isPacmanHitClyde = false;

        private double inkyPosX;
        private double blinkyPosX;
        private double pinkyPosX;
        private double clydePosX;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuAnimationVisual"/> class.
        /// </summary>
        public MainMenuAnimationVisual()
        {
            this.bilagit = new BilagitModel(this);
            this.pacman = new PacmanModel(this);
            this.inky = new InkyModel(this);
            this.blinky = new BlinkyModel(this);
            this.pinky = new PinkyModel(this);
            this.clyde = new ClydeModel(this);

            this.pacman.CurrentDirection = 1;
            this.inky.CurrentDirection = 1;
            this.blinky.CurrentDirection = 1;
            this.pinky.CurrentDirection = 1;
            this.clyde.CurrentDirection = 1;

            this.pacman.Position = new Point(-100, 50);
            this.bilagit.Position = new Point(350, 55);
            this.inky.Position = new Point(-200, 50);
            this.blinky.Position = new Point(-235, 50);
            this.pinky.Position = new Point(-270, 50);
            this.clyde.Position = new Point(-305, 50);

            this.Loaded += this.OnLoaded;
        }

        /// <summary>
        /// Override OnRender method to show visual on window.
        /// </summary>
        /// <param name="drawingContext">DrawingContext element, to draw out elements.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawImage(this.bilagit.Sprite, new Rect(this.bilagit.Position.X, this.bilagit.Position.Y, this.bilagit.Size + 10, this.bilagit.Size + 10));

            drawingContext.DrawImage(this.pacman.Sprite, new Rect(this.pacman.Position.X, this.pacman.Position.Y, this.pacman.Size * 2, this.pacman.Size * 2));

            if (!this.isPacmanHitInky)
            {
                drawingContext.DrawImage(this.inky.Sprite, new Rect(this.inky.Position.X, this.inky.Position.Y, this.inky.Size * 2, this.inky.Size * 2));
            }

            if (!this.isPacmanHitBlinky)
            {
                drawingContext.DrawImage(this.blinky.Sprite, new Rect(this.blinky.Position.X, this.blinky.Position.Y, this.blinky.Size * 2, this.blinky.Size * 2));
            }

            if (!this.isPacmanHitPinky)
            {
                drawingContext.DrawImage(this.pinky.Sprite, new Rect(this.pinky.Position.X, this.pinky.Position.Y, this.pinky.Size * 2, this.pinky.Size * 2));
            }

            if (!this.isPacmanHitClyde)
            {
                drawingContext.DrawImage(this.clyde.Sprite, new Rect(this.clyde.Position.X, this.clyde.Position.Y, this.clyde.Size * 2, this.clyde.Size * 2));
            }
        }

        /// <summary>
        /// Event which rises when the window loaded.
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.dt = new DispatcherTimer();
            this.dt.Interval = new TimeSpan(0, 0, 0, 0, 50);
            this.dt.Tick += this.Update;
            this.dt.Start();

            this.animation = new DispatcherTimer();
            this.animation.Interval = new TimeSpan(0, 0, 0, 0, 50);
            this.animation.Tick += this.UpdateAnimation;
            this.animation.Start();

            this.InvalidateVisual();
        }

        private void UpdateAnimation(object sender, EventArgs e)
        {
            if (this.pacman.Collide(new BilagitModel(this) { Position = new Point(380, 50) }))
            {
                this.isPacmanHitBilagit = true;
                this.pacman.CurrentDirection = 2;

                this.inkyPosX = this.inky.Position.X;
                this.blinkyPosX = this.blinky.Position.X;
                this.pinkyPosX = this.pinky.Position.X;
                this.clydePosX = this.clyde.Position.X;

                this.clyde = new CrazyGhost(this);
                this.inky = new CrazyGhost(this);
                this.pinky = new CrazyGhost(this);
                this.blinky = new CrazyGhost(this);

                this.inky.Position = new Point(this.inkyPosX, 50);
                this.blinky.Position = new Point(this.blinkyPosX, 50);
                this.pinky.Position = new Point(this.pinkyPosX, 50);
                this.clyde.Position = new Point(this.clydePosX, 50);

                this.inky.CurrentDirection = 2;
                this.blinky.CurrentDirection = 2;
                this.pinky.CurrentDirection = 2;
                this.clyde.CurrentDirection = 2;
            }

            if (this.isPacmanHitBilagit)
            {
                this.bilagit.Sprite = null;

                this.pacman.Position = new Point(this.pacman.Position.X - SPEED, this.pacman.Position.Y);

                if (this.pacman.Collide(new InkyModel(this) { Position = new Point(this.inky.Position.X - 30, 50) }))
                {
                    this.isPacmanHitInky = true;
                }

                if (this.pacman.Collide(new PinkyModel(this) { Position = new Point(this.pinky.Position.X - 30, 50) }))
                {
                    this.isPacmanHitPinky = true;
                }

                if (this.pacman.Collide(new BlinkyModel(this) { Position = new Point(this.blinky.Position.X - 30, 50) }))
                {
                    this.isPacmanHitBlinky = true;
                }

                if (this.pacman.Collide(new ClydeModel(this) { Position = new Point(this.clyde.Position.X - 30, 50) }))
                {
                    this.isPacmanHitClyde = true;
                }

                if (!this.isPacmanHitInky)
                {
                    this.inky.Position = new Point(this.inky.Position.X - (SPEED / 3), this.inky.Position.Y);
                }

                if (!this.isPacmanHitBlinky)
                {
                    this.blinky.Position = new Point(this.blinky.Position.X - (SPEED / 3), this.blinky.Position.Y);
                }

                if (!this.isPacmanHitPinky)
                {
                    this.pinky.Position = new Point(this.pinky.Position.X - (SPEED / 3), this.pinky.Position.Y);
                }

                if (!this.isPacmanHitClyde)
                {
                    this.clyde.Position = new Point(this.clyde.Position.X - (SPEED / 3), this.clyde.Position.Y);
                }
            }
            else
            {
                this.pacman.Position = new Point(this.pacman.Position.X + SPEED, this.pacman.Position.Y);

                this.inky.Position = new Point(this.inky.Position.X + SPEED, this.inky.Position.Y);
                this.blinky.Position = new Point(this.blinky.Position.X + SPEED, this.blinky.Position.Y);
                this.pinky.Position = new Point(this.pinky.Position.X + SPEED, this.pinky.Position.Y);
                this.clyde.Position = new Point(this.clyde.Position.X + SPEED, this.clyde.Position.Y);
            }

            this.InvalidateVisual();
        }

        private void Update(object sender, EventArgs e)
        {
        }
    }
}
