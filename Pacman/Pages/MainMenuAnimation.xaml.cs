﻿// <copyright file="MainMenuAnimation.xaml.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman.Pages
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MainMenuAnimationVisual.xaml
    /// </summary>
    public partial class MainMenuAnimationVisual : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuAnimationVisual"/> class.
        /// </summary>
        public MainMenuAnimationVisual()
        {
            this.InitializeComponent();
        }
    }
}
