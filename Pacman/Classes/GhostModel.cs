﻿// <copyright file="GhostModel.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Extends <see cref="DynamicObject"/> class, to represent a ghost.
    /// </summary>
    public abstract class GhostModel : DynamicObject
    {
        /// <summary>
        /// Contains the sprites of the ghost in the right direction.
        /// </summary>
        private ImageSource[] ghostSpritesRight;

        /// <summary>
        /// Contains the sprites of the ghost in the left direction.
        /// </summary>
        private ImageSource[] ghostSpritesLeft;

        /// <summary>
        /// Contains the sprites of the ghost in the up direction.
        /// </summary>
        private ImageSource[] ghostSpritesUp;

        /// <summary>
        /// Contains the sprites of the ghost in the down direction.
        /// </summary>
        private ImageSource[] ghostSpritesDown;

        private int ghostX;

        private int ghostY;

        /// <summary>
        /// Initializes a new instance of the <see cref="GhostModel"/> class.
        /// </summary>
        /// <param name="frame">Parameter to give a FrameworkElement, which contains visuals.</param>
        protected GhostModel(FrameworkElement frame)
            : base(frame)
        {
            this.Size = 16;
            this.Geometry = new EllipseGeometry(new Point(this.Position.X, this.Position.Y), this.Size, this.Size);

            this.GhostSpritesRight = new ImageSource[2];
            this.GhostSpritesLeft = new ImageSource[2];
            this.GhostSpritesUp = new ImageSource[2];
            this.GhostSpritesDown = new ImageSource[2];

            this.GhostX = 0;
            this.GhostY = 0;

            // this.dt = new DispatcherTimer();
            // this.dt.Interval = new TimeSpan(0, 0, 0, 0, 200);
            // this.dt.Tick += this.Update;
            // this.dt.Start();
        }

        /// <summary>
        /// Gets or sets the sprites of the ghost in the right direction
        /// </summary>
        public ImageSource[] GhostSpritesRight
        {
            get
            {
                return this.ghostSpritesRight;
            }

            set
            {
                this.ghostSpritesRight = value;
            }
        }

        /// <summary>
        /// Gets or sets the sprites of the ghost in the left direction.
        /// </summary>
        public ImageSource[] GhostSpritesLeft
        {
            get
            {
                return this.ghostSpritesLeft;
            }

            set
            {
                this.ghostSpritesLeft = value;
            }
        }

        /// <summary>
        /// Gets or sets the sprites of the ghost in the up direction
        /// </summary>
        public ImageSource[] GhostSpritesUp
        {
            get
            {
                return this.ghostSpritesUp;
            }

            set
            {
                this.ghostSpritesUp = value;
            }
        }

        /// <summary>
        /// Gets or sets the sprites of the ghost in the down direction
        /// </summary>
        public ImageSource[] GhostSpritesDown
        {
            get
            {
                return this.ghostSpritesDown;
            }

            set
            {
                this.ghostSpritesDown = value;
            }
        }

        /// <summary>
        /// Gets or sets the X-coordinate of ghost.
        /// </summary>
        public int GhostX
        {
            get
            {
                return this.ghostX;
            }

            set
            {
                this.ghostX = value;
            }
        }

        /// <summary>
        /// Gets or sets the Y-coordinate of ghost.
        /// </summary>
        public int GhostY
        {
            get
            {
                return this.ghostY;
            }

            set
            {
                this.ghostY = value;
            }
        }

        /// <summary>
        /// Method to update sprite of ghosts, depending on its direction.
        /// </summary>
        public void UpdateSprite()
        {
            if (this.CurrentDirection == 1)
            {
                this.Sprite = this.GhostSpritesRight[0];
            }
            else if (this.CurrentDirection == 2)
            {
                this.Sprite = this.GhostSpritesLeft[0];
            }
            else if (this.CurrentDirection == 3)
            {
                this.Sprite = this.GhostSpritesUp[0];
            }
            else if (this.CurrentDirection == 4)
            {
                this.Sprite = this.GhostSpritesDown[0];
            }
        }

        private void Update(object sender, EventArgs e)
        {
            this.UpdateSprite();
        }
    }
}
