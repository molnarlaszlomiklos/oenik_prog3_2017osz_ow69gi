﻿// <copyright file="BilagitModel.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Implements bilagit, extends <see cref="FoodModel"/>.
    /// </summary>
    public class BilagitModel : FoodModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BilagitModel"/> class.
        /// </summary>
        /// <param name="frame">Parameter to give a FrameworkElement, which contains visuals.</param>
        public BilagitModel(FrameworkElement frame)
            : base(frame)
        {
            this.Size = 14;
            this.Geometry = new EllipseGeometry(new Point(0, 0), this.Size, this.Size);
            this.Sprite = new ImageReader().LoadImage("bilagit.png");
        }
    }
}
