﻿// <copyright file="MapReader.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System;
    using System.IO;
    using System.Text;
    using System.Windows;

    /// <summary>
    /// Reads a map from a specified file.
    /// </summary>
    public static class MapReader
    {
        /// <summary>
        /// Constant for cheese of a pixel on the map.
        /// </summary>
        public const int Cheese = 0;

        /// <summary>
        /// Constant for bilagit of a pixel on the map.
        /// </summary>
        public const int Bilagit = 1;

        /// <summary>
        /// Constant for wall of a pixel on the map.
        /// </summary>
        public const int Wall = 2;

        /// <summary>
        /// Constant for empty pixel on the map.
        /// </summary>
        public const int Empty = 3;

        /// <summary>
        /// Constant for the starter point of Pacman.
        /// </summary>
        public const int StartPacman = 4;

        /// <summary>
        /// Constant for the starter point of Inky.
        /// </summary>
        public const int StartInky = 5;

        /// <summary>
        /// Constant for the starter point of Blinky.
        /// </summary>
        public const int StartBlinky = 6;

        /// <summary>
        /// Constant for the starter point of Pinky.
        /// </summary>
        public const int StartPinky = 7;

        /// <summary>
        /// Constant for the starter point of Clyde.
        /// </summary>
        public const int StartClyde = 8;

        /// <summary>
        /// Constant for the left teleportpoint.
        /// </summary>
        public const int Tpv = 9;

        /// <summary>
        /// Constant for the right teleportpoint.
        /// </summary>
        public const int Tpw = 10;

        /// <summary>
        /// Loads a specific map from a file.
        /// </summary>
        /// <param name="path">String, where the mapfile is allocated.</param>
        /// <param name="countCheese">Out Int, number of the cheese-pixels.</param>
        /// <param name="startPacman">Starter point of Pacman.</param>
        /// <param name="startInky">Starter point of Inky.</param>
        /// <param name="startBlinky">Starter point of Blinky.</param>
        /// <param name="startPinky">Starter point of Pinky.</param>
        /// <param name="startClyde">Starter point of Clyde.</param>
        /// <param name="tpv">Location of the left teleportpoint.</param>
        /// <param name="tpw">Location of the right teleportpoint.</param>
        /// <returns>Returns the matrix of the map.</returns>
        public static int[,] LoadMap(string path, out int countCheese, out Point startPacman, out Point startInky, out Point startBlinky, out Point startPinky, out Point startClyde, out Point tpv, out Point tpw)
        {
            string[] raw = File.ReadAllLines(path, Encoding.UTF8);
            int[,] map = new int[raw[0].Length, raw.Length];

            countCheese = 0;
            startPacman = new Point(0, 0);
            startPinky = new Point(0, 0);
            startBlinky = new Point(0, 0);
            startInky = new Point(0, 0);
            startClyde = new Point(0, 0);

            tpv = new Point(0, 0);
            tpw = new Point(0, 0);

            for (int y = 0; y < map.GetLength(1); y++)
            {
                for (int x = 0; x < map.GetLength(0); x++)
                {
                    switch (raw[y][x])
                    {
                        default: map[x, y] = Empty; break;
                        case '0': map[x, y] = Cheese; countCheese++; break;
                        case '1': map[x, y] = Bilagit; break;
                        case '2': map[x, y] = Wall; break;
                        case '3': map[x, y] = Empty; break;
                        case 'M': map[x, y] = StartPacman; startPacman = new Point(x, y); break;
                        case 'I': map[x, y] = StartInky; startInky = new Point(x, y); break;
                        case 'B': map[x, y] = StartBlinky; startBlinky = new Point(x, y); break;
                        case 'P': map[x, y] = StartPinky; startPinky = new Point(x, y); break;
                        case 'C': map[x, y] = StartClyde; startClyde = new Point(x, y); break;
                        case 'v': map[x, y] = Tpv; tpv = new Point(x, y); break;
                        case 'w': map[x, y] = Tpw; tpw = new Point(x, y); break;
                    }
                }
            }

            return map;
        }
    }
}
