﻿// <copyright file="GameObject.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Abstract class, represents game objects.
    /// </summary>
    public class GameObject
    {
        /// <summary>
        /// Static random number.
        /// </summary>
        private static Random rand = new Random();

        /// <summary>
        /// Sprite of object.
        /// </summary>
        private ImageSource sprite;

        /// <summary>
        /// Size of object.
        /// </summary>
        private double size;

        /// <summary>
        /// Position of object.
        /// </summary>
        private Point position;

        /// <summary>
        /// Geometry of object.
        /// </summary>
        private Geometry geometry;

        /// <summary>
        /// Rectangle of the object.
        /// </summary>
        private Rect location;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameObject"/> class.
        /// </summary>
        /// <param name="frame">Parameter to give a FrameworkElement, which contains visuals.</param>
        protected GameObject(FrameworkElement frame)
        {
            this.Size = 10;
        }

        /// <summary>
        /// Gets or sets a random number.
        /// </summary>
        public static Random Rand
        {
            get
            {
                return rand;
            }

            set
            {
                rand = value;
            }
        }

        /// <summary>
        /// Gets or sets the sprite of object.
        /// </summary>
        public ImageSource Sprite
        {
            get
            {
                return this.sprite;
            }

            set
            {
                this.sprite = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of object.
        /// </summary>
        public double Size
        {
            get
            {
                return this.size;
            }

            set
            {
                this.size = value;
            }
        }

        /// <summary>
        /// Gets or sets the position of object.
        /// </summary>
        public Point Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
            }
        }

        /// <summary>
        /// Gets or sets the geometry of object.
        /// </summary>
        public Geometry Geometry
        {
            get
            {
                return this.geometry;
            }

            set
            {
                this.geometry = value;
            }
        }

        /// <summary>
        /// Gets the geometry of object.
        /// </summary>
        public Geometry GetGeometry
        {
            get
            {
                this.geometry.Transform = new TranslateTransform(this.position.X, this.position.Y);
                return this.geometry;
            }
        }

        /// <summary>
        /// Gets or sets the rectangle of the object.
        /// </summary>
        public Rect Location
        {
            get
            {
                return this.location;
            }

            set
            {
                this.location = value;
            }
        }

        /// <summary>
        /// Method to check collisions.
        /// </summary>
        /// <param name="other">An object check collisions with.</param>
        /// <returns>Returns True if the the intersect of two object is bigger than 0, else false.</returns>
        public bool Collide(GameObject other)
        {
            PathGeometry intersection = Geometry.Combine(
                this.GetGeometry.GetFlattenedPathGeometry(),
                other.GetGeometry.GetFlattenedPathGeometry(),
                GeometryCombineMode.Intersect,
                null);

            return intersection.GetArea() > 0;
        }
    }
}
