﻿// <copyright file="CheeseModel.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Extends <see cref="FoodModel"/> to represent cheese objects.
    /// </summary>
    public class CheeseModel : FoodModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CheeseModel"/> class.
        /// </summary>
        /// <param name="frame">Parameter to give a FrameworkElement, which contains visuals.</param>
        public CheeseModel(FrameworkElement frame)
            : base(frame)
        {
            this.Size = 12;
            this.Geometry = new RectangleGeometry(new Rect(new Size(this.Size, this.Size)));
            this.Sprite = new ImageReader().LoadImage("cheese.png");
        }
    }
}
