﻿// <copyright file="CrazyGhost.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows;

    /// <summary>
    /// Represent a crazy ghost, extends <see cref="GhostModel"/>
    /// </summary>
    public class CrazyGhost : GhostModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrazyGhost"/> class.
        /// </summary>
        /// <param name="frame">Parameter to give a FrameworkElement, which contains visuals.</param>
        public CrazyGhost(FrameworkElement frame)
            : base(frame)
        {
            this.GhostSpritesRight[0] = new ImageReader().LoadImage("crazy.png");
            this.GhostSpritesRight[1] = new ImageReader().LoadImage("crazy.png");

            this.GhostSpritesLeft[0] = new ImageReader().LoadImage("crazy.png");
            this.GhostSpritesLeft[1] = new ImageReader().LoadImage("crazy.png");

            this.GhostSpritesUp[0] = new ImageReader().LoadImage("crazy.png");
            this.GhostSpritesUp[1] = new ImageReader().LoadImage("crazy.png");

            this.GhostSpritesDown[0] = new ImageReader().LoadImage("crazy.png");
            this.GhostSpritesDown[1] = new ImageReader().LoadImage("crazy.png");

            this.Sprite = new ImageReader().LoadImage("crazy.png");
        }
    }
}
