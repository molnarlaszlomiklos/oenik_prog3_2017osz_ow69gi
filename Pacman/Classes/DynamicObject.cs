﻿// <copyright file="DynamicObject.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows;
    using System.Windows.Threading;

    /// <summary>
    /// Represents a dynamic object, extends <see cref="GameObject"/>.
    /// </summary>
    public abstract class DynamicObject : GameObject
    {
        /// <summary>
        /// Instance of FrameworkElement.
        /// </summary>
        private FrameworkElement frame;

        /// <summary>
        /// Field of currect direction of the object.
        /// </summary>
        private int currentDirection;

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicObject"/> class.
        /// </summary>
        /// <param name="frame">Parameter to give a FrameworkElement, which contains visuals.</param>
        protected DynamicObject(FrameworkElement frame)
            : base(frame)
        {
        }

        /// <summary>
        /// Gets or sets the current direction of the object.
        /// </summary>
        public int CurrentDirection
        {
            get
            {
                return this.currentDirection;
            }

            set
            {
                this.currentDirection = value;
            }
        }

        /// <summary>
        /// Gets the maximum distance to the edge of the screen.
        /// </summary>
        protected double MaxDistanceToScreenEdge
        {
            get
            {
                return (this.frame.ActualWidth > this.frame.ActualHeight) ? this.frame.ActualWidth : this.frame.ActualHeight;
            }
        }

        /// <summary>
        /// Gets the center of the screen.
        /// </summary>
        protected Point CenterOfScreen
        {
            get
            {
                return new Point(this.frame.ActualWidth / 2, this.frame.ActualHeight / 2);
            }
        }
    }
}
