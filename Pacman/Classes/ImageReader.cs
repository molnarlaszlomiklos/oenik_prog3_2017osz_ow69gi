﻿// <copyright file="ImageReader.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Read an image from the filename. Image must be located in Resources/Images directory.
    /// </summary>
    public class ImageReader
    {
        /// <summary>
        /// Size of an image object.
        /// </summary>
        public const int Size = 28;

        /// <summary>
        /// Loaded an image file.
        /// </summary>
        /// <param name="filename">Filename of the image.</param>
        /// <returns>BitmapImage</returns>
        public ImageSource LoadImage(string filename)
        {
            return new BitmapImage(new Uri(@"pack://application:,,,/Resources/Images/" + filename));
        }
    }
}
