﻿// <copyright file="FoodModel.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows;

    /// <summary>
    /// Extends <see cref="GameObject"/> class, to represent a food.
    /// </summary>
    public abstract class FoodModel : GameObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FoodModel"/> class.
        /// </summary>
        /// <param name="frame">Parameter to give a FrameworkElement, which contains visuals.</param>
        protected FoodModel(FrameworkElement frame)
            : base(frame)
        {
        }
    }
}
