﻿// <copyright file="BlinkyModel.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows;

    /// <summary>
    /// Extends <see cref="GhostModel"/> to represent specific ghost, called Blinky.
    /// </summary>
    public class BlinkyModel : GhostModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BlinkyModel"/> class.
        /// </summary>
        /// <param name="frame">Parameter to give a FrameworkElement, which contains visuals.</param>
        public BlinkyModel(FrameworkElement frame)
            : base(frame)
        {
            this.GhostSpritesRight[0] = new ImageReader().LoadImage("blinky_right_1.png");
            this.GhostSpritesRight[1] = new ImageReader().LoadImage("blinky_right_2.png");

            this.GhostSpritesLeft[0] = new ImageReader().LoadImage("blinky_left_1.png");
            this.GhostSpritesLeft[1] = new ImageReader().LoadImage("blinky_left_2.png");

            this.GhostSpritesUp[0] = new ImageReader().LoadImage("blinky_up_1.png");
            this.GhostSpritesUp[1] = new ImageReader().LoadImage("blinky_up_2.png");

            this.GhostSpritesDown[0] = new ImageReader().LoadImage("blinky_down_1.png");
            this.GhostSpritesDown[1] = new ImageReader().LoadImage("blinky_down_2.png");

            this.Sprite = this.GhostSpritesRight[0];
        }
    }
}
