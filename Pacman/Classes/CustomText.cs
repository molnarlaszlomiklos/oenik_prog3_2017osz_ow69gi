﻿// <copyright file="CustomText.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Represent custom text with custom font.
    /// </summary>
    public class CustomText
    {
        /// <summary>
        /// Returns a FormattedText, print a text in custom style.
        /// </summary>
        /// <param name="content">Content of the text to be printed.</param>
        /// <param name="size">Fontsize of the text to be printed.</param>
        /// <param name="brush">Color of the text to be printed.</param>
        /// <returns>FormattedText</returns>
        public FormattedText TextContent(string content, int size, Brush brush)
        {
            return new FormattedText(
                content,
                CultureInfo.InvariantCulture,
                FlowDirection.LeftToRight,
                new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "/Resources/Fonts/#ArcadeClassic"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal),
                size,
                brush);
        }
    }
}
