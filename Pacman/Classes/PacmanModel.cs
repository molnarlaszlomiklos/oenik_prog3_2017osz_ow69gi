﻿// <copyright file="PacmanModel.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// Implement Pacman.
    /// </summary>
    public class PacmanModel : DynamicObject
    {
        private DispatcherTimer dt;
        private ImageSource[] pacmanSpritesRight;
        private ImageSource[] pacmanSpritesLeft;
        private ImageSource[] pacmanSpritesUp;
        private ImageSource[] pacmanSpritesDown;

        /// <summary>
        /// Initializes a new instance of the <see cref="PacmanModel"/> class.
        /// </summary>
        /// <param name="frame">Frame of base</param>
        public PacmanModel(FrameworkElement frame)
            : base(frame)
        {
            this.Size = 16;
            this.Geometry = new EllipseGeometry(new Point(this.Position.X, this.Position.Y), this.Size, this.Size);

            this.Location = new Rect(this.Position.X * this.Size, this.Position.Y * this.Size, this.Size, this.Size);

            this.pacmanSpritesRight = new ImageSource[4];
            this.pacmanSpritesRight[0] = new ImageReader().LoadImage("pacman_right_full.png");
            this.pacmanSpritesRight[1] = new ImageReader().LoadImage("pacman_right_half.png");
            this.pacmanSpritesRight[2] = new ImageReader().LoadImage("pacman_right_opened.png");
            this.pacmanSpritesRight[3] = new ImageReader().LoadImage("pacman_right_half.png");

            this.pacmanSpritesLeft = new ImageSource[4];
            this.pacmanSpritesLeft[0] = new ImageReader().LoadImage("pacman_left_full.png");
            this.pacmanSpritesLeft[1] = new ImageReader().LoadImage("pacman_left_half.png");
            this.pacmanSpritesLeft[2] = new ImageReader().LoadImage("pacman_left_opened.png");
            this.pacmanSpritesLeft[3] = new ImageReader().LoadImage("pacman_left_half.png");

            this.pacmanSpritesUp = new ImageSource[4];
            this.pacmanSpritesUp[0] = new ImageReader().LoadImage("pacman_top_full.png");
            this.pacmanSpritesUp[1] = new ImageReader().LoadImage("pacman_top_half.png");
            this.pacmanSpritesUp[2] = new ImageReader().LoadImage("pacman_top_opened.png");
            this.pacmanSpritesUp[3] = new ImageReader().LoadImage("pacman_top_half.png");

            this.pacmanSpritesDown = new ImageSource[4];
            this.pacmanSpritesDown[0] = new ImageReader().LoadImage("pacman_bottom_full.png");
            this.pacmanSpritesDown[1] = new ImageReader().LoadImage("pacman_bottom_half.png");
            this.pacmanSpritesDown[2] = new ImageReader().LoadImage("pacman_bottom_opened.png");
            this.pacmanSpritesDown[3] = new ImageReader().LoadImage("pacman_bottom_half.png");

            this.Sprite = this.pacmanSpritesRight[0];

            this.dt = new DispatcherTimer();
            this.dt.Interval = new TimeSpan(0, 0, 0, 0, 120);
            this.dt.Tick += this.Update;
            this.dt.Start();
        }

        /// <summary>
        /// Moving Pacman.
        /// </summary>
        public async void Move()
        {
            if (this.CurrentDirection == 1)
            {
                foreach (ImageSource imgsrc in this.pacmanSpritesRight)
                {
                    this.Sprite = imgsrc;
                    await Task.Delay(30);
                }
            }
            else if (this.CurrentDirection == 2)
            {
                foreach (ImageSource imgsrc in this.pacmanSpritesLeft)
                {
                    this.Sprite = imgsrc;
                    await Task.Delay(30);
                }
            }
            else if (this.CurrentDirection == 3)
            {
                foreach (ImageSource imgsrc in this.pacmanSpritesUp)
                {
                    this.Sprite = imgsrc;
                    await Task.Delay(30);
                }
            }
            else if (this.CurrentDirection == 4)
            {
                foreach (ImageSource imgsrc in this.pacmanSpritesDown)
                {
                    this.Sprite = imgsrc;
                    await Task.Delay(30);
                }
            }
        }

        private void Update(object sender, EventArgs e)
        {
            this.Move();
        }
    }
}
