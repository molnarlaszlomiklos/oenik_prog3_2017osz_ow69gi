﻿// <copyright file="InkyModel.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows;

    /// <summary>
    /// Extends <see cref="GhostModel"/> to represent ghost Inky.
    /// </summary>
    public class InkyModel : GhostModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InkyModel"/> class.
        /// </summary>
        /// <param name="frame">Parameter to give a FrameworkElement, which contains visuals.</param>
        public InkyModel(FrameworkElement frame)
            : base(frame)
        {
            this.GhostSpritesRight[0] = new ImageReader().LoadImage("inky_right_1.png");
            this.GhostSpritesRight[1] = new ImageReader().LoadImage("inky_right_2.png");

            this.GhostSpritesLeft[0] = new ImageReader().LoadImage("inky_left_1.png");
            this.GhostSpritesLeft[1] = new ImageReader().LoadImage("inky_left_2.png");

            this.GhostSpritesUp[0] = new ImageReader().LoadImage("inky_up_1.png");
            this.GhostSpritesUp[1] = new ImageReader().LoadImage("inky_up_2.png");

            this.GhostSpritesDown[0] = new ImageReader().LoadImage("inky_down_1.png");
            this.GhostSpritesDown[1] = new ImageReader().LoadImage("inky_down_2.png");

            this.Sprite = this.GhostSpritesRight[0];
        }
    }
}