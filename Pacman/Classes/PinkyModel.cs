﻿// <copyright file="PinkyModel.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows;

    /// <summary>
    /// Extends <see cref="GhostModel"/> to represent ghost Pinky.
    /// </summary>
    public class PinkyModel : GhostModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PinkyModel"/> class.
        /// </summary>
        /// <param name="frame">Parameter to give a FrameworkElement, which contains visuals.</param>
        public PinkyModel(FrameworkElement frame)
            : base(frame)
        {
            this.GhostSpritesRight[0] = new ImageReader().LoadImage("pinky_right_1.png");
            this.GhostSpritesRight[1] = new ImageReader().LoadImage("pinky_right_2.png");

            this.GhostSpritesLeft[0] = new ImageReader().LoadImage("pinky_left_1.png");
            this.GhostSpritesLeft[1] = new ImageReader().LoadImage("pinky_left_2.png");

            this.GhostSpritesUp[0] = new ImageReader().LoadImage("pinky_up_1.png");
            this.GhostSpritesUp[1] = new ImageReader().LoadImage("pinky_up_2.png");

            this.GhostSpritesDown[0] = new ImageReader().LoadImage("pinky_down_1.png");
            this.GhostSpritesDown[1] = new ImageReader().LoadImage("pinky_down_2.png");

            this.Sprite = this.GhostSpritesRight[0];
        }
    }
}
