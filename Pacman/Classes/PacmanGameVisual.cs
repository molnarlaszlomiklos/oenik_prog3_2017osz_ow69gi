﻿// <copyright file="PacmanGameVisual.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// Creates the game.
    /// </summary>
    public class PacmanGameVisual : FrameworkElement
    {
        private const int SPEED = 1;
        private Random rand = new Random();

        private int[,] map;

        private int lives;
        private int score;
        private int countCheese;
        private bool crazyGhost;

        private PacmanModel pacman;
        private InkyModel inky;
        private BlinkyModel blinky;
        private PinkyModel pinky;
        private ClydeModel clyde;

        private Point startpacman;
        private Point startinky;
        private Point startblinky;
        private Point startpinky;
        private Point startclyde;
        private Point tpV;
        private Point tpW;

        private DispatcherTimer dt;
        private DispatcherTimer countDown;

        private int counter;

        private int playerX;
        private int playerY;

        /// <summary>
        /// Initializes a new instance of the <see cref="PacmanGameVisual"/> class.
        /// </summary>
        public PacmanGameVisual()
        {
            this.lives = 3;
            this.score = 0;
            this.countCheese = 0;
            this.crazyGhost = false;
            this.counter = 10;

            this.Map = MapReader.LoadMap(@"../../Resources/Maps/map.map", out this.countCheese, out this.startpacman, out this.startinky, out this.startblinky, out this.startpinky, out this.startclyde, out this.tpV, out this.tpW);

            this.pacman = new PacmanModel(this);
            this.inky = new InkyModel(this);
            this.blinky = new BlinkyModel(this);
            this.pinky = new PinkyModel(this);
            this.clyde = new ClydeModel(this);

            this.pacman.Position = this.startpacman;
            this.inky.Position = this.startinky;
            this.blinky.Position = this.startblinky;
            this.pinky.Position = this.startpinky;
            this.clyde.Position = this.startclyde;

            this.playerX = (int)this.startpacman.X;
            this.playerY = (int)this.startpacman.Y;

            this.inky.GhostX = (int)this.startinky.X;
            this.inky.GhostY = (int)this.startinky.Y;

            this.blinky.GhostX = (int)this.startblinky.X;
            this.blinky.GhostY = (int)this.startblinky.Y;

            this.pinky.GhostX = (int)this.startpinky.X;
            this.pinky.GhostY = (int)this.startpinky.Y;

            this.clyde.GhostX = (int)this.startclyde.X;
            this.clyde.GhostY = (int)this.startclyde.Y;

            this.Loaded += this.OnLoaded;
        }

        /// <summary>
        /// Gets or sets the two-dimensional array of the map.
        /// </summary>
        public int[,] Map
        {
            get
            {
                return this.map;
            }

            set
            {
                this.map = value;
            }
        }

        /// <summary>
        /// Override the OnRender call to add a drawingvisuals to offsetpanel.
        /// </summary>
        /// <param name="drawingContext">The drawing instructions for a specific element. This context is provided to the layout system.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawText(new CustomText().TextContent("HIGHSCORE", 24, Brushes.White), new Point(0, 500));
            drawingContext.DrawText(new CustomText().TextContent(this.score.ToString(), 24, Brushes.White), new Point(130,  500));
            drawingContext.DrawImage(new ImageReader().LoadImage("board.bmp"), new Rect(0, 0, 450, 500));

            for (int y = 0; y < this.map.GetLength(1); y++)
            {
                for (int x = 0; x < this.map.GetLength(0); x++)
                {
                    if (this.map[x, y] == MapReader.Cheese)
                    {
                        drawingContext.DrawImage(new CheeseModel(this).Sprite, new Rect((x * 16) + 4, (y * 16) + 5, 9, 10));
                    }

                    if (this.map[x, y] == MapReader.Bilagit)
                    {
                        drawingContext.DrawImage(new BilagitModel(this).Sprite, new Rect(x * 16, (y * 16) + 5, 16, 16));
                    }

                    if (this.map[x, y] == MapReader.Tpv)
                    {
                        drawingContext.DrawImage(new ImageReader().LoadImage("tp.png"), new Rect((x * 16) - 15, (y * 16) - 5, 30, 30));
                    }

                    if (this.map[x, y] == MapReader.Tpw)
                    {
                        drawingContext.DrawImage(new ImageReader().LoadImage("tp.png"), new Rect(x * 16, (y * 16) - 5, 30, 30));
                    }
                }
            }

            for (int i = 0; i < this.lives; i++)
            {
                drawingContext.DrawImage(new ImageReader().LoadImage("pacman_left_half.png"), new Rect((i * 21) + 5, 530, 20, 20));
            }

            drawingContext.DrawImage(this.pacman.Sprite, this.pacman.Location);

            if (this.crazyGhost)
            {
                drawingContext.DrawImage(new CrazyGhost(this).Sprite, this.inky.Location);
                drawingContext.DrawImage(new CrazyGhost(this).Sprite, this.blinky.Location);
                drawingContext.DrawImage(new CrazyGhost(this).Sprite, this.pinky.Location);
                drawingContext.DrawImage(new CrazyGhost(this).Sprite, this.clyde.Location);
            }
            else
            {
                drawingContext.DrawImage(this.inky.Sprite, this.inky.Location);
                drawingContext.DrawImage(this.blinky.Sprite, this.blinky.Location);
                drawingContext.DrawImage(this.pinky.Sprite, this.pinky.Location);
                drawingContext.DrawImage(this.clyde.Sprite, this.clyde.Location);
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).KeyDown += this.OnKeyDown;

            this.dt = new DispatcherTimer();
            this.dt.Interval = new TimeSpan(0, 0, 0, 0, 150);
            this.dt.Tick += this.Update;
            this.dt.Start();

            this.countDown = new DispatcherTimer();
            this.countDown.Interval = new TimeSpan(0, 0, 0, 1);
            this.countDown.Tick += this.OnCountDown;
            
            this.pacman.CurrentDirection = 2;

            this.pinky.CurrentDirection = 3;
            this.inky.CurrentDirection = 3;
            this.blinky.CurrentDirection = 1;
            this.clyde.CurrentDirection = 3;
        }

        private void OnCountDown(object sender, EventArgs e)
        {
            this.counter--;
            if (this.counter == 0)
            {
                this.countDown.Stop();
                this.crazyGhost = false;
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left && this.map[this.playerX - SPEED, this.playerY] != MapReader.Wall && this.playerX - 1 >= 0)
            {
                this.pacman.CurrentDirection = 2;
            }
            else if (e.Key == Key.Up && this.map[this.playerX, this.playerY - SPEED] != MapReader.Wall && this.playerY - 1 >= 0)
            {
                this.pacman.CurrentDirection = 3;
            }
            else if (e.Key == Key.Right && this.map[this.playerX + SPEED, this.playerY] != MapReader.Wall && this.playerX + 1 < this.map.GetLength(0))
            {
                this.pacman.CurrentDirection = 1;
            }
            else if (e.Key == Key.Down && this.map[this.playerX, this.playerY + SPEED] != MapReader.Wall && this.playerY + 1 < this.map.GetLength(1))
            {
                this.pacman.CurrentDirection = 4;
            }
        }

        private void Update(object sender, EventArgs e)
        {
            this.MovePacman();
            this.MoveGhost(this.blinky);
            this.MoveGhost(this.inky);
            this.MoveGhost(this.pinky);
            this.MoveGhost(this.clyde);

            this.pacman.Location = new Rect(this.pacman.Position.X * this.pacman.Size, this.pacman.Position.Y * this.pacman.Size, this.pacman.Size, this.pacman.Size);

            this.inky.Location = new Rect(this.inky.Position.X * this.inky.Size, this.inky.Position.Y * this.inky.Size, this.inky.Size, this.inky.Size);
            this.blinky.Location = new Rect(this.blinky.Position.X * this.blinky.Size, this.blinky.Position.Y * this.blinky.Size, this.blinky.Size, this.blinky.Size);
            this.pinky.Location = new Rect(this.pinky.Position.X * this.pinky.Size, this.pinky.Position.Y * this.pinky.Size, this.pinky.Size, this.pinky.Size);
            this.clyde.Location = new Rect(this.clyde.Position.X * this.clyde.Size, this.clyde.Position.Y * this.clyde.Size, this.clyde.Size, this.clyde.Size);

            this.InvalidateVisual();
        }

        private void MoveGhost(GhostModel ghost)
        {
            if (ghost.CurrentDirection == 1 && this.map[ghost.GhostX + SPEED, ghost.GhostY] != MapReader.Wall && ghost.GhostX + 1 < this.map.GetLength(0))
            {
                ghost.Position = new Point(ghost.Position.X + SPEED, ghost.Position.Y);
                ghost.GhostX += SPEED;
            }
            else if (ghost.CurrentDirection == 2 && this.map[ghost.GhostX - SPEED, ghost.GhostY] != MapReader.Wall && ghost.GhostX - 1 >= 0)
            {
                ghost.Position = new Point(ghost.Position.X - SPEED, ghost.Position.Y);
                ghost.GhostX -= SPEED;
            }
            else if (ghost.CurrentDirection == 3 && this.map[ghost.GhostX, ghost.GhostY - SPEED] != MapReader.Wall && ghost.GhostY - 1 >= 0)
            {
                ghost.Position = new Point(ghost.Position.X, ghost.Position.Y - SPEED);
                ghost.GhostY -= SPEED;
            }
            else if (ghost.CurrentDirection == 4 && this.map[ghost.GhostX, ghost.GhostY + SPEED] != MapReader.Wall && ghost.GhostY + 1 < this.map.GetLength(1))
            {
                ghost.Position = new Point(ghost.Position.X, ghost.Position.Y + SPEED);
                ghost.GhostY += SPEED;
            }

            if (this.map[ghost.GhostX + 1, ghost.GhostY] == MapReader.Tpv)
            {
                ghost.Position = new Point(this.tpW.X - 1, this.tpW.Y);
                ghost.GhostX = (int)this.tpW.X - 1;
                ghost.GhostY = (int)this.tpW.Y;
            }

            if (this.map[ghost.GhostX, ghost.GhostY] == MapReader.Tpw)
            {
                ghost.Position = new Point(this.tpV.X, this.tpV.Y);
                ghost.GhostX = (int)this.tpV.X;
                ghost.GhostY = (int)this.tpV.Y;
            }

            if (ghost.GhostX == this.playerX && ghost.GhostY == this.playerY)
            {
                if (!this.crazyGhost)
                {
                    this.lives--;
                    if (this.lives == 0)
                    {
                        MessageBox.Show("Game over\r\nScore: " + this.score, "Game Over", MessageBoxButton.OK);
                        this.Map = MapReader.LoadMap(@"../../Resources/Maps/map.map", out this.countCheese, out this.startpacman, out this.startinky, out this.startblinky, out this.startpinky, out this.startclyde, out this.tpV, out this.tpW);

                        this.score = 0;
                        this.lives = 3;

                        this.pacman.Position = this.startpacman;
                        this.inky.Position = this.startinky;
                        this.blinky.Position = this.startblinky;
                        this.pinky.Position = this.startpinky;
                        this.clyde.Position = this.startclyde;

                        this.playerX = (int)this.startpacman.X;
                        this.playerY = (int)this.startpacman.Y;

                        this.inky.GhostX = (int)this.startinky.X;
                        this.inky.GhostY = (int)this.startinky.Y;

                        this.blinky.GhostX = (int)this.startblinky.X;
                        this.blinky.GhostY = (int)this.startblinky.Y;

                        this.pinky.GhostX = (int)this.startpinky.X;
                        this.pinky.GhostY = (int)this.startpinky.Y;

                        this.clyde.GhostX = (int)this.startclyde.X;
                        this.clyde.GhostY = (int)this.startclyde.Y;
                    }

                    this.pacman.CurrentDirection = 2;

                    this.pacman.Position = this.startpacman;
                    this.blinky.Position = this.startblinky;
                    this.inky.Position = this.startinky;
                    this.pinky.Position = this.startpinky;
                    this.clyde.Position = this.startclyde;

                    this.playerX = (int)this.startpacman.X;
                    this.playerY = (int)this.startpacman.Y;

                    this.inky.GhostX = (int)this.startinky.X;
                    this.inky.GhostY = (int)this.startinky.Y;

                    this.blinky.GhostX = (int)this.startblinky.X;
                    this.blinky.GhostY = (int)this.startblinky.Y;

                    this.pinky.GhostX = (int)this.startpinky.X;
                    this.pinky.GhostY = (int)this.startpinky.Y;

                    this.clyde.GhostX = (int)this.startclyde.X;
                    this.clyde.GhostY = (int)this.startclyde.Y;
                }
            }

            // Begin AI
            if (ghost.CurrentDirection == 1)
            {
                if (this.map[ghost.GhostX, ghost.GhostY + 1] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall)
                {
                    int x = this.rand.Next(90);
                    if (x < 30)
                    {
                        ghost.CurrentDirection = 1;
                    }
                    else if (x > 60)
                    {
                        ghost.CurrentDirection = 3;
                    }
                    else
                    {
                        ghost.CurrentDirection = 4;
                    }
                }
                else if (this.map[ghost.GhostX, ghost.GhostY - 1] == MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY + 1] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 1;
                    }
                    else
                    {
                        ghost.CurrentDirection = 4;
                    }
                }
                else if (this.map[ghost.GhostX, ghost.GhostY + 1] == MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 1;
                    }
                    else
                    {
                        ghost.CurrentDirection = 3;
                    }
                }
                else if (this.map[ghost.GhostX, ghost.GhostY + 1] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] == MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 4;
                    }
                    else
                    {
                        ghost.CurrentDirection = 3;
                    }
                }
                else if (this.map[ghost.GhostX + 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY + 1] == MapReader.Wall)
                {
                    ghost.CurrentDirection = 3;
                }
                else if (this.map[ghost.GhostX + 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY + 1] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] == MapReader.Wall)
                {
                    ghost.CurrentDirection = 4;
                }
            }
            else if (ghost.CurrentDirection == 2)
            {
                if (this.map[ghost.GhostX, ghost.GhostY + 1] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall)
                {
                    int x = this.rand.Next(90);
                    if (x < 30)
                    {
                        ghost.CurrentDirection = 2;
                    }
                    else if (x > 60)
                    {
                        ghost.CurrentDirection = 3;
                    }
                    else
                    {
                        ghost.CurrentDirection = 4;
                    }
                }
                else if (this.map[ghost.GhostX, ghost.GhostY - 1] == MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY + 1] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 2;
                    }
                    else
                    {
                        ghost.CurrentDirection = 4;
                    }
                }
                else if (this.map[ghost.GhostX, ghost.GhostY + 1] == MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 2;
                    }
                    else
                    {
                        ghost.CurrentDirection = 3;
                    }
                }
                else if (this.map[ghost.GhostX - 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY + 1] != MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 4;
                    }
                    else
                    {
                        ghost.CurrentDirection = 3;
                    }
                }
                else if (this.map[ghost.GhostX - 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY + 1] == MapReader.Wall)
                {
                    ghost.CurrentDirection = 3;
                }
                else if (this.map[ghost.GhostX - 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY + 1] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] == MapReader.Wall)
                {
                    ghost.CurrentDirection = 4;
                }
            }
            else if (ghost.CurrentDirection == 3)
            {
                if (this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX - 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall)
                {
                    int x = this.rand.Next(90);
                    if (x < 30)
                    {
                        ghost.CurrentDirection = 2;
                    }
                    else if (x > 60)
                    {
                        ghost.CurrentDirection = 3;
                    }
                    else
                    {
                        ghost.CurrentDirection = 1;
                    }
                }
                else if (this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX - 1, ghost.GhostY] == MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 1;
                    }
                    else
                    {
                        ghost.CurrentDirection = 3;
                    }
                }
                else if (this.map[ghost.GhostX + 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall && this.map[ghost.GhostX - 1, ghost.GhostY] != MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 2;
                    }
                    else
                    {
                        ghost.CurrentDirection = 3;
                    }
                }
                else if (this.map[ghost.GhostX, ghost.GhostY - 1] == MapReader.Wall && this.map[ghost.GhostX - 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 1;
                    }
                    else
                    {
                        ghost.CurrentDirection = 2;
                    }
                }
                else if (this.map[ghost.GhostX - 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] == MapReader.Wall)
                {
                    ghost.CurrentDirection = 1;
                }
                else if (this.map[ghost.GhostX + 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX - 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] == MapReader.Wall)
                {
                    ghost.CurrentDirection = 2;
                }
            }
            else if (ghost.CurrentDirection == 4)
            {
                if (this.map[ghost.GhostX, ghost.GhostY + 1] != MapReader.Wall && this.map[ghost.GhostX - 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall)
                {
                    int x = this.rand.Next(90);
                    if (x < 30)
                    {
                        ghost.CurrentDirection = 2;
                    }
                    else if (x > 60)
                    {
                        ghost.CurrentDirection = 4;
                    }
                    else
                    {
                        ghost.CurrentDirection = 1;
                    }
                }
                else if (this.map[ghost.GhostX, ghost.GhostY + 1] == MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX - 1, ghost.GhostY] != MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 1;
                    }
                    else
                    {
                        ghost.CurrentDirection = 2;
                    }
                }
                else if (this.map[ghost.GhostX + 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX - 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY + 1] != MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 4;
                    }
                    else
                    {
                        ghost.CurrentDirection = 2;
                    }
                }
                else if (this.map[ghost.GhostX - 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY - 1] != MapReader.Wall)
                {
                    if (this.rand.Next(100) < 50)
                    {
                        ghost.CurrentDirection = 4;
                    }
                    else
                    {
                        ghost.CurrentDirection = 1;
                    }
                }
                else if (this.map[ghost.GhostX - 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX + 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY + 1] == MapReader.Wall)
                {
                    ghost.CurrentDirection = 1;
                }
                else if (this.map[ghost.GhostX + 1, ghost.GhostY] == MapReader.Wall && this.map[ghost.GhostX - 1, ghost.GhostY] != MapReader.Wall && this.map[ghost.GhostX, ghost.GhostY + 1] == MapReader.Wall)
                {
                    ghost.CurrentDirection = 2;
                }
            }

            // End AI
        }

        private void MovePacman()
        {
            if (this.pacman.CurrentDirection == 1 && this.map[this.playerX + SPEED, this.playerY] != MapReader.Wall && this.playerX + 1 < this.map.GetLength(0))
            {
                this.pacman.Position = new Point(this.pacman.Position.X + SPEED, this.pacman.Position.Y);
                this.playerX += SPEED;
            }
            else if (this.pacman.CurrentDirection == 2 && this.map[this.playerX - SPEED, this.playerY] != MapReader.Wall && this.playerX - 1 >= 0)
            {
                this.pacman.Position = new Point(this.pacman.Position.X - SPEED, this.pacman.Position.Y);
                this.playerX -= SPEED;
            }
            else if (this.pacman.CurrentDirection == 3 && this.map[this.playerX, this.playerY - SPEED] != MapReader.Wall && this.playerY - 1 >= 0)
            {
                this.pacman.Position = new Point(this.pacman.Position.X, this.pacman.Position.Y - SPEED);
                this.playerY -= SPEED;
            }
            else if (this.pacman.CurrentDirection == 4 && this.map[this.playerX, this.playerY + SPEED] != MapReader.Wall && this.playerY + 1 < this.map.GetLength(1))
            {
                this.pacman.Position = new Point(this.pacman.Position.X, this.pacman.Position.Y + SPEED);
                this.playerY += SPEED;
            }

            if (this.map[this.playerX, this.playerY] == MapReader.Cheese)
            {
                this.score += 10;
                this.map[this.playerX, this.playerY] = MapReader.Empty;
                this.countCheese--;
                if (this.countCheese == 0)
                {
                    MessageBox.Show("Win! Score: " + this.score, "You win!", MessageBoxButton.OK);
                    Application.Current.Shutdown();
                }
            }

            if (this.map[this.playerX + 1, this.playerY] == MapReader.Tpv)
            {
                this.pacman.Position = new Point(this.tpW.X - 1, this.tpW.Y);
                this.playerX = (int)this.tpW.X - 1;
                this.playerY = (int)this.tpW.Y;
            }

            if (this.map[this.playerX, this.playerY] == MapReader.Tpw)
            {
                this.pacman.Position = new Point(this.tpV.X, this.tpV.Y);
                this.playerX = (int)this.tpV.X;
                this.playerY = (int)this.tpV.Y;
            }

            if (this.map[this.playerX, this.playerY] == MapReader.Bilagit)
            {
                this.score += 50;
                this.map[this.playerX, this.playerY] = MapReader.Empty;
                this.crazyGhost = true;
                this.counter = 10;
                this.countDown.Start();
            }

            if (this.playerX == this.inky.GhostX && this.playerY == this.inky.GhostY && this.crazyGhost)
            {
                this.score += 350;
                this.inky.Position = this.startinky;
                this.inky.GhostX = (int)this.startinky.X;
                this.inky.GhostY = (int)this.startinky.Y;
            }

            if (this.playerX == this.blinky.GhostX && this.playerY == this.blinky.GhostY && this.crazyGhost)
            {
                this.score += 350;
                this.blinky.Position = this.startblinky;
                this.blinky.GhostX = (int)this.startblinky.X;
                this.blinky.GhostY = (int)this.startblinky.Y;
            }

            if (this.playerX == this.pinky.GhostX && this.playerY == this.pinky.GhostY && this.crazyGhost)
            {
                this.score += 350;
                this.pinky.Position = this.startpinky;
                this.pinky.GhostX = (int)this.startpinky.X;
                this.pinky.GhostY = (int)this.startpinky.Y;
            }

            if (this.playerX == this.clyde.GhostX && this.playerY == this.clyde.GhostY && this.crazyGhost)
            {
                this.score += 350;
                this.clyde.Position = this.startclyde;
                this.clyde.GhostX = (int)this.startclyde.X;
                this.clyde.GhostY = (int)this.startclyde.Y;
            }
        }
    }
}