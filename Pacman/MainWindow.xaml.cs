﻿// <copyright file="MainWindow.xaml.cs" company="MLMStudio">
// Copyright (c) MLMStudio. All rights reserved.
// </copyright>

namespace Pacman
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.Content = new MainMenu(this);
        }

        /// <summary>
        /// Set a page to open
        /// </summary>
        /// <param name="page">Given page to open</param>
        public void SetPage(Page page)
        {
            this.Content = page;
        }
    }
}
